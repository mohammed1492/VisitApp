# VisitApp
 
 ### Functionalitites: 
 - Add new Visit
 - Modify existing Visit
 - Delete existing Visit
 - Visits ordered decsending by date
 - Searching for all visits of the same visitor
 - Sorting 
 - Seperating visits into categories: today, past, future
 - Checkin time
 - Checkout time
 - Autofill existing visitor info if existed, when adding new visit
 - Email is sent to the visitor. 


 ### Constraint: 
 - The edit button is activated if and only if one visit is selected
 - The delete button can have multiple visits selected
 - Checkout button is disabled unitl the visit is checked in
 - If the visit is checked out it cannot be modefied
 

 ### Further work 
 - Connecting to a real smtp server with a proper email body, The idea is to make a barcode that is sent to the visitor with the visit Id
 - Connecting the application to LDAP or MS Identity for user authentication, and seperating the roles of user. Ideally, we shall have two roles: Admin have the ability to add and view visits, and User can only add new visits and view its visits 
 - Connecting the application with ERP system to read and link employee IDs 
 - Using the same email function, another trigger can be configured whenever the PATCH checkin request is sent the backend, the server will send an email to the employee that his visitor have arrived. 
