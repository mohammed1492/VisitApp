﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
namespace VisitApp.Models
{
    public class DataContext : DbContext
    {
        // Constructor
        public DataContext(DbContextOptions<DataContext> opts) : base(opts) { }


        public DbSet<Employee> employees => Set<Employee>();
        public DbSet<Visitor> visitors => Set<Visitor>();
        public DbSet<Visit> visits => Set<Visit>();

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Visitor>()
                .HasIndex(u => u.NationalId)
                .IsUnique();
        }
        protected override void ConfigureConventions(ModelConfigurationBuilder builder)
        {
            builder.Properties<DateOnly>()
                .HaveConversion<DateOnlyConverter>()
                .HaveColumnType("Date");
        }

    }

    public class DateOnlyConverter : ValueConverter<DateOnly, DateTime>
      {
          /// <summary>
          /// Creates a new instance of this converter.
          /// </summary>
          public DateOnlyConverter() : base(
                  d => d.ToDateTime(TimeOnly.MinValue),
                  d => DateOnly.FromDateTime(d))
          { }
      }
}
