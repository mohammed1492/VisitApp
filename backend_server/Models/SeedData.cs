﻿using Microsoft.EntityFrameworkCore;

namespace VisitApp.Models
{
    public static class SeedData
    {
        public static void SeedDatabase(DataContext context)
        {
            
            context.Database.Migrate(); 
            if(context.visitors.Count() == 0 && context.employees.Count() == 0 && context.visits.Count() == 0)
            {
                Employee employee = new Employee { Name = "Mohammed Ahmed"};
                Employee security = new Employee {Name = "Saleh Ali" ,  IsSecurityOfficer = true};
                Employee employee1 = new Employee { Name = "Ibrahim Khalid"};

           

                context.employees.Add(security);
                //context.visitors.AddRange(

                Visitor v1 = new Visitor
                {
                    NationalId = 1101101010,
                    Name = "Abdullah Ibrahim",
                    Phone = "0543791972",
                    Email = "mail1@mail.com"
                };
                Visitor v2 = new Visitor
                {
                    NationalId = 1682101010,
                    Name = "Aziz Mohammed",
                    Phone = "0538363873",
                    Email = "mail2@mail.com",

                };

                context.visits.AddRange(
                    new Visit
                    {
                        Date = new DateTime(2022, 6, 11),
                        Time = new TimeSpan(9, 30, 0),
                        FloorNumber = 3,
                        VisitedEmployee = employee,
                        TheVisitor = v1
                    },
                    new Visit
                    {
                        Date = new DateTime(2022, 7, 23), 
                        Time = new TimeSpan(10, 0, 0), 
                        FloorNumber = 8, 
                        VisitedEmployee = employee1, 
                        TheVisitor = v2
                    }
                    ); 
            

                  
                // context.employees.Add(employee1);
                 
                context.SaveChanges(); 
            }
        }
    }
}
