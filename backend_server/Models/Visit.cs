﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VisitApp.Models
{
    public class Visit
    {
        [Key]
        public long VisitId { get; set; }

        public int FloorNumber { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }

        [DataType(DataType.Time)]
        public TimeSpan Time { get; set; }

        [DataType(DataType.Time)]
        public TimeSpan CheckInTime { get; set; }

        [DataType(DataType.Time)]
        public TimeSpan CheckOutTime { get; set; }

        public Employee? VisitedEmployee { get; set; }
        public long VisitedEmployeeId { get; set; }
        public Visitor? TheVisitor { get; set; }
        public long TheVisitorId    { get; set; }

    }
}
