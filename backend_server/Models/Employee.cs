﻿using System.ComponentModel.DataAnnotations;

namespace VisitApp.Models
{
    public class Employee
    {

        [Key]
        public long Id  { get; set; }
        public string? Name { get; set; }
        public bool IsSecurityOfficer{ get; set; } = false;

    }
}