﻿
using System.ComponentModel.DataAnnotations;

namespace VisitApp.Models
{
    public class IdentityModel
    {
        public string? UserName { get; set; }
        public string? Password { get; set; }   
    }
}
