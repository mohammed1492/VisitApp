﻿using VisitApp.Models;
namespace VisitApp
{
    public class TestMiddleware
    {
        private RequestDelegate nextDelegate;

        public TestMiddleware(RequestDelegate next)
        {
            nextDelegate = next;
        }

        public async Task Invoke(HttpContext context, DataContext dataContext)
        {
            if(context.Request.Path == "/test")
            {
                await context.Response.WriteAsync(
                    $"There are {dataContext.employees.Count()} employees\n");
                await context.Response.WriteAsync(
                    $"There are {dataContext.visitors.Count()} visitors\n");
            }
            else
            {
                await nextDelegate(context);
            }
        }
    }
}