﻿using System.ComponentModel.DataAnnotations;
namespace VisitApp.Helpers
{
    public class FormData
    {

        // public long vId { get; set; } // automatically generated
        public long vNationalId { get; set; }
        public string? vName { get; set; }
        public string? vPhone { get; set; }
        public string? vEmail { get; set; }

        public int VisitingFloor { get; set; }

        [DataType(DataType.Date)]
        public DateTime Date { get; set; }
        public TimeSpan Time { get; set; }
        public long eId { get; set; }


    }

    public class UpdateDataVisitor
    {
        public long vNationalId { get; set; }
        public string? vName { get; set; }
        public string? vPhone { get; set; }
        public string? vEmail { get; set; }
    }
    public class UpdateDataVisit
    {

        public int VisitingFloor { get; set; }
        public long eId { get; set; }

        [DataType(DataType.Date)]
        public DateTime Date { get; set; }
        public TimeSpan Time { get; set; }
        


    }
}
