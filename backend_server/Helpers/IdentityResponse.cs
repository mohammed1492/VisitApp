﻿namespace VisitApp.Helpers
{
    public class IdentityResponse
    {
        public string? Status { get; set; }
        public string? Message { get; set;  } 
    }
}
