﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using VisitApp.Models;
using System.Net;
using System.Net.Mail;
using VisitApp.Helpers;



namespace VisitApp.Controllers
{
    //[Authorize]
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class VisitController : Controller
    {
        private object JoidedData; 
        private DataContext dataContext;

        public VisitController(DataContext cxt)
        {
            dataContext = cxt;
            JoidedData = FetchData(); 
        }

        # region Private Methods
        private void ReFetchData(){
            JoidedData = FetchData(); 
        }
        private Object FetchData()
        {
            var list = dataContext.visitors
                .Join(
                     dataContext.visits,
                     visit => visit.Id,
                     visitor => visitor.TheVisitorId,
                     (visitor, visit) => new { visitor, visit }
                 )
                .Join(
                    dataContext.employees,
                    combined => combined.visit.VisitedEmployeeId,
                    employee => employee.Id,
                    (combined, employee) => new
                    {
                        VisitId = combined.visit.VisitId,
                        vId = combined.visitor.Id,
                        vName = combined.visitor.Name,
                        vNationalId = combined.visitor.NationalId,
                        vPhone = combined.visitor.Phone,
                        vEmail = combined.visitor.Email,
                        eName = employee.Name,
                        eId = employee.Id,
                        VisitingFloor = combined.visit.FloorNumber,
                        Date = combined.visit.Date.ToString().Substring(0, 11),
                        Time = combined.visit.Time.ToString().Substring(0, 5),
                        CheckIn = combined.visit.CheckInTime.ToString().Substring(0, 8),
                        CheckOut = combined.visit.CheckOutTime.ToString().Substring(0, 8),
                    }

                ).OrderByDescending((s) => s.Date);
            return list;
        }
        private long GetVisitorId(long nationalId)
        {
            var obj = dataContext.visitors.Where(row => row.NationalId == nationalId).Single();
            if (obj == null)
                return 0;
            else
                return obj.Id;
        }
        # endregion

        # region HttpGet
        [HttpGet]
        public IActionResult GetVisits(){
            if(JoidedData == null)
                return NotFound(); 
            else
                return Ok(JoidedData); 
        }

        [HttpGet]
        [Route("[action]/{date}")]
        public IActionResult GetVisitsOfDay(string date){
            var result = dataContext.visitors
                .Join(
                     dataContext.visits,
                     visit => visit.Id,
                     visitor => visitor.TheVisitorId,
                     (visitor, visit) => new { visitor, visit }
                 )
                .Join(
                    dataContext.employees,
                    combined => combined.visit.VisitedEmployeeId,
                    employee => employee.Id,
                    (combined, employee) => new
                    {
                        VisitId = combined.visit.VisitId,
                        vId = combined.visitor.Id,
                        vName = combined.visitor.Name,
                        vNationalId = combined.visitor.NationalId,
                        vPhone = combined.visitor.Phone,
                        vEmail = combined.visitor.Email,
                        eName = employee.Name,
                        eId = employee.Id,
                        VisitingFloor = combined.visit.FloorNumber,
                        Date = combined.visit.Date.ToString().Substring(0, 11),
                        Time = combined.visit.Time.ToString().Substring(0, 5),
                        CheckIn = combined.visit.CheckInTime.ToString().Substring(0, 8),
                        CheckOut = combined.visit.CheckOutTime.ToString().Substring(0, 8),
                    }

                ).Where(combined => combined.Date == date && combined.CheckOut == "00:00:00");
                if(!result.Any()){
                    return NotFound("There are not visit on"+date); 
                }
                return Ok(result);
        }

        [HttpGet("{nationalId}")]
        public IActionResult GetVisit(long nationalId)
        {
            var obj = dataContext.visitors
                .Join(
                     dataContext.visits,
                     visit => visit.Id,
                     visitor => visitor.TheVisitorId,
                     (visitor, visit) => new { visitor, visit }
                 )
                .Join(
                    dataContext.employees,
                    combined => combined.visit.VisitedEmployeeId,
                    employee => employee.Id,
                    (combined, employee) => new
                    {
                        VisitId = combined.visit.VisitId,
                        vId = combined.visitor.Id,
                        vName = combined.visitor.Name,
                        vNationalId = combined.visitor.NationalId,
                        vPhone = combined.visitor.Phone,
                        vEmail = combined.visitor.Email,
                        eName = employee.Name,
                        eId = employee.Id,
                        VisitingFloor = combined.visit.FloorNumber,
                        Date = combined.visit.Date.ToString().Substring(0, 11),
                        Time = combined.visit.Time.ToString().Substring(0, 8),
                        CheckIn = combined.visit.CheckInTime.ToString().Substring(0, 8),
                        CheckOut = combined.visit.CheckOutTime.ToString().Substring(0, 8),
                    }

                ).Where(combined => combined.vNationalId == nationalId);

            if (!obj.Any())
                return NotFound();
            else
                return Ok(obj);
        }
        //This will return only the visitor
        [Route("[action]/{nationalId}")]
        [HttpGet]  // to prevent URL request
        public IActionResult GetVisitor(long nationalId)
        {
            var obj = dataContext.visitors.Where(row => row.NationalId == nationalId);
            if (obj == null)
                return NotFound();
            else
                return Ok(obj);
        }
        # endregion

        # region HttpPost
        [HttpPost]
        public async Task<IActionResult> AddVisitCombined([FromBody] FormData data)
        {
            Visitor? newVisitor;    // A place holder if needed
            var existObject = dataContext.visitors.Where(visitor => visitor.NationalId == data.vNationalId);
            if (!existObject.Any())
            {
                // if the visitor does not exist will crete new visitor
                newVisitor = new Visitor
                {
                    NationalId = data.vNationalId,
                    Name = data.vName,
                    Email = data.vEmail,
                    Phone = data.vPhone
                };
            }
            else
            {
                newVisitor = existObject.First();
            }

            try{
                dataContext.visits.Where(visit => visit.TheVisitorId == GetVisitorId(data.vNationalId) && visit.Date == data.Date).Any();

            }catch(Exception exp)  
            {
                dataContext.visitors.Add(newVisitor); 
            }

            await dataContext.visits.AddAsync(new Visit{
                FloorNumber = data.VisitingFloor, 
                Date = data.Date, 
                Time = data.Time,
                VisitedEmployeeId = data.eId, 
                TheVisitor = newVisitor
            }); 
            var affectedDBRows = await dataContext.SaveChangesAsync(); 
            if (affectedDBRows > 0){
                //Relay.SendEmail(newVisitor.Email, "Visit Confiramtion", "Your visit is confirmed"); 
                return Ok(affectedDBRows);
            }   
            // return BadRequest();
            return NoContent();
        }
        # endregion

        # region HttpPut
        [HttpPut("[action]/{visitId}")]
        public async Task<IActionResult> UpdateVisit(long visitId, [FromBody] UpdateDataVisit data)
        {
            Visit? found = await dataContext.visits.Where(v => v.VisitId == visitId).FirstOrDefaultAsync();
            if (found != null)
            {
                found.FloorNumber = data.VisitingFloor;
                found.Date = data.Date;
                found.Time = data.Time;
                found.VisitedEmployeeId = data.eId; 
                await dataContext.SaveChangesAsync();
                 
                return Ok();
            }

            return NotFound();
        }
        [HttpPut("[action]/{visitorId}")]
        public async Task<IActionResult> UpdateVisitor(long visitorId, [FromBody] UpdateDataVisitor data){
            Visitor? found = await dataContext.visitors.Where(visitor => visitor.Id == visitorId).FirstOrDefaultAsync();
            if(found != null){
                found.NationalId = data.vNationalId; 
                found.Name = data.vName; 
                found.Email = data.vEmail; 
                found.Phone = data.vPhone; 
                await dataContext.SaveChangesAsync();
                 
                return Ok();
            }
            return NotFound();
        }
        # endregion

        # region HttpPatch
        [HttpPatch("{method}/{visitId}")]
        public async Task<IActionResult> PatchVisit(long visitId, string method)
        {
            Visit? found = await dataContext.visits.Where(v => v.VisitId == visitId).FirstOrDefaultAsync();
            if (found != null)
            {
                if (method.Equals("Checkin"))
                    found.CheckInTime = DateTime.Now.TimeOfDay;
                else if (method.Equals("Checkout"))
                    found.CheckOutTime = DateTime.Now.TimeOfDay;
                await dataContext.SaveChangesAsync();
                 
                return Ok();
            }
            return NotFound();
        }
        #endregion

        #region HttpDelete
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteVisit(long id)
        {
            var itemToRemove = await dataContext.visits.SingleOrDefaultAsync(x => x.VisitId == id); //returns a single item.

            if (itemToRemove != null)
            {
                dataContext.visits.Remove(itemToRemove);
                dataContext.SaveChanges();
                
                return Ok();

            }

            return NotFound();
        }
        #endregion

    }

    class Relay
    {
        private const string SenderEmail = "";
        private const string SenderPassword = "";
        private const string SmptServerAddress = "smtp-mail.outlook.com"; 
        private const int    SmptPort = 587; 

        public static void SendEmail(string To, string Subject, string Body)
        {
            string From = SenderEmail;
            MailMessage mail = new MailMessage(From, To);


            mail.Subject = Subject;
            mail.Body = Body;


            SmtpClient client = new SmtpClient(SmptServerAddress, SmptPort);
            client.Credentials = new System.Net.NetworkCredential()
            {
                UserName = SenderEmail,
                Password = SenderPassword
            };
            client.EnableSsl = true;
            client.Send(mail);


        }
    }
}

