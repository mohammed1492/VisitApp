﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace VisitApp.Migrations
{
    public partial class updated : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_visitors_NationalId",
                table: "visitors",
                column: "NationalId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_visitors_NationalId",
                table: "visitors");
        }
    }
}
