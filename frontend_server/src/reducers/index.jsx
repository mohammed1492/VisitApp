import UserReducer from './UserReducer'
import CommentReducer from './CommentReducer'
import {combineReducers} from 'redux'

const RootReducer = combineReducers({
    UserReducer,
    CommentReducer
})

export default RootReducer