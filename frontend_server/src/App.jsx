import logo from './logo.svg';
import './App.css';
import { useState, useEffect } from 'react'

import Nav from './components/Nav'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import Login from './pages/Login'
import Home from './pages/Home'

function App() {
  const [authToken, setAuthToken] = useState('');
  // const apiAddress = 'http://localhost:5000/'
  // const setToken = (token) => {
  //   setAuthToken(token)
  // }
  useEffect(() => {
    var token = localStorage.getItem('token')
    if (token != authToken) 
      setAuthToken(token)
  }, [localStorage])
  return (
    <>
      <BrowserRouter>
        <Nav />
        <Routes>
          <Route
            exact path="/"
            element={
              <Home
                authToken={authToken}
              />
            }
          />
          <Route path="/login" element={<Login/>} />
        </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;
