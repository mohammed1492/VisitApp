import React from 'react'
import { useState, SyntheticEvent } from 'react'
import {Navigate} from 'react-router-dom'
import axios from 'axios'
// import api from '../config'
const Login = ({}) => {
    const [username, setUsername] = useState('') ;
    const [password, setPassword] = useState('') ;
    const [redirect, setRedirect] = useState(false); 
    const apiAddress =  global.config.i18n.IdentityApiAdress;

    const onSubmit = async (e) =>  {
        e.preventDefault(); 

        if(!username || !password){
            alert('Please check login parameters !!')
            return 
        }
        // const data = JSON.stringify({
        //     userName: username, 
        //     Password: password
        // })

        const header = {
            'Content-Type': 'application/json'
        }
       

        
        var token = await axios.post(
            apiAddress+'login', 
            {
                username, 
                password
            }, 
        ).then(res => res.data ).then(data => {
            localStorage.setItem("token", data.token)
            localStorage.setItem("expire", data.expiration)
            localStorage.setItem("actor", data.roles)
            setRedirect(true); 
        }).catch(function(error){
            alert(error)
            return
        })
        // setAuthToken()
        
        
    };
    if(redirect){
        return <Navigate to="/" />
    }
    return (
        <div className='container login-container'>
            <form className='login-form' onSubmit={onSubmit}>
                <div className='form-control'>
                    <label htmlFor="username">Username</label>
                    <input type="text" id="username" value={username} onChange={(e) => setUsername(e.target.value)}/>
                    <label htmlFor="password">Password</label>
                    <input type="password" id="password" value={password} onChange={(e) => setPassword(e.target.value)}/>
                    <input type="submit" value="Login" className='btn btn-secondary'
                    /> 
                </div>
            </form>
        </div>
    );
};

export default Login