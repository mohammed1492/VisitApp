import React from 'react'

const Admin = () => {
    return (
        <div className='container home-container'>
            <Datatable token={token} data={data}
                heading={heading} />

            <Button id='formButton' color='blue' text='Add new Visitor'
                onClick={(e) => setVisibility(!visibility)} />
            <CustomPopup
                onClose={popupCloseHandler}
                show={visibility}
                title="Hello Jeetendra"
            >
                <VisitorForm token={token} onClose={(e) => setVisibility(false)} />
            </CustomPopup>
        </div>
    )

    
}

export default Admin