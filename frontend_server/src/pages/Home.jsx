import React from 'react'
import Datatable from '../components/Datatable'
import { useState, useEffect } from 'react'
import Button from '../components/Button'
import { Navigate } from 'react-router-dom'
import PopupForm from '../components/PopupForm'

import axios from 'axios'
import Buttons from '../components/Buttons'
import MainContent from '../components/MainContent'

const Home = (authToken) => {
  const [data, setData] = useState()
  const [tokenString] = useState(authToken.authToken)
  const [searchText, setSearchText] = useState()

  const apiAddress = global.config.i18n.visitApiAddress; 
  async function fetchTable() {
    var today = new Date().toISOString().slice(0, 10)
    var tableData = await axios.get(
      apiAddress,
      {
        headers: {
          Authorization: `Bearer ${tokenString}`
        }
      })
      .then(res => res.data).then(data => {
        return setData(data)
      })
      .catch(function (error) {
        return error
      })
  }

  useEffect(() => {
    // setLoading(true)
    fetchTable()
    // setLoading(false)
    // setType(typeof(data))
    // setData(cloneOf(data))
  }, [])



  const findVisitor = async (e) => {
    e.preventDefault()
    var tableData = await axios.get(
      apiAddress+searchText,
      {
        headers: {
          Authorization: `Bearer ${tokenString}`
        }
      })
      .then(res => res.data).then(data => {
        return setData(data)
      })
      .catch(function (error) {
        return error
      })
  }



  return (


    <div className='container home-container'>
      {/* <img src="C:\Users\moham\OneDrive\Desktop\logo.png" /> */}

      <form class="search-box" role="search">
        <input class="form-control" type="search" placeholder="البحث برقم الهوية" value={searchText}
          onChange={(e) => setSearchText(e.target.value)} />
        <Button text='بحث' color='#40C1AC' onClick={(e) => findVisitor(e)} id='searchBtn' />
      </form>
      {tokenString != null ? <MainContent
        data={data}
        tokenString={tokenString}
        fetchTable={fetchTable}
      /> : <h3>Unauthorized Access: please Login</h3>}
    </div>
  )
}




export default Home