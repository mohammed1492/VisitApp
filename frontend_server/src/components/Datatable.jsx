import { render } from '@testing-library/react';
import DataTable, { memoize } from 'react-data-table-component';
import Button from './Button'
import { useState, useEffect, useCallback } from 'react'
import React from 'react'
import Buttons from './Buttons'
// import Button from './Button'
import VisitorForm from './VisitorForm'
import { confirm } from 'react-confirm-box'
import { Tab, Tabs } from 'react-bootstrap'


// import Table from './Datatable';
import axios from 'axios';
import PopupForm from './PopupForm';
import _default from 'react-bootstrap/esm/Accordion';

const Datatable = ({ token, data, setFormVisibility, setFormData, setUpdatePressed }) => {
  // const [data, setData] = useState([])
  const [selectedRows, setSelectedRows] = useState()
  const [toggleClear, setToggleClear] = useState(true);
  const [refresh, setRefresh] = useState(false)
  const [pastVisits, setPastVisit] = useState()
  const [futureVisits, setFutureVisits] = useState()
  const [todayVisits, setTodayVisits] = useState()
  const [tableDataHolder, setTableDataHolder] = useState(data)
  const [tabKey, setTabKey] = useState('todayVisits')
  const apiAddress = global.config.i18n.visitApiAddress;

  // const [showCheckOutBtn, setShowCheckOutBtn] = useState(false)
  const today = new Date().toISOString().slice(0, 10)
  useEffect(() => {

    if (data) {
      setPastVisit(data.filter(
        function (el) {
          return el.checkIn != "00:00:00" &&
            el.checkOut != "00:00:00"
        }
      ))

      setFutureVisits(data.filter(
        function (el) {
          return el.checkIn === "00:00:00" &&
            el.checkOut === "00:00:00" && el.date != today
        }
      ))
    }

  }, [data])

  useEffect(() => {
    axios.get(
      apiAddress + 'GetVisitsOfDay/' + today,
      {
        headers: {
          Authorization: `Bearer ${token}`
        }
      }
    ).then(res => res.data).then(data => {
      setTodayVisits(data)
      return data
    })
      .catch(function (error) {
        return error
      })
  }, [])

  const handleRowSelected = ({ selectedRows }) => {
    setSelectedRows(selectedRows);
    setToggleClear(false)
    // console.log(typeof(selectedRows))

  };

  const onAdd = () => {
    setUpdatePressed(false)
    setFormVisibility(true)

  }

  const Delete = () => {
    setRefresh(false)

    // var id = selectedRows[0].visitId
    selectedRows.map((row) => {
      var id = row.visitId
      axios.delete(
        apiAddress + id,
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        }
      ).then(res => res.data).then(data => {
        setRefresh(true)
        return data
      })
        .catch(function (error) {
          return error
        })
    })
  }

  const onDelete = async (options) => {

    if (Object.keys(selectedRows).length > 0) 
      selectedRows.map((row) => {
        if (row.checkIn != "00:00:00" && row.checkOut != "00:00:00") {
          alert(' لا يمكن حذف الزيارة'+row.visitId)
          return
        }
        else {
          if (window.confirm("هل أنت متأكد من حذف " + Object.keys(selectedRows).length + " زيارة"))
            Delete()
        }
      })
    else
      alert('لم يتم اختيار زيارة للحذف')
  }

  const onEdit = () => {
    if (Object.keys(selectedRows).length === 1) {
      var row = selectedRows[0];
      if (row.checkIn != "00:00:00" && row.checkOut != "00:00:00") {
        alert('لا يمكن تعديل بيانات الزيارة')
        return
      }
      else {
        setFormData(row)
        setUpdatePressed(true)
        setFormVisibility(true)
        console.log(row.visitId)
      }

    }
    else {
      alert('يجبب اختيار زيارة واحدة فقط للتعديل')
    }
  }

  if (refresh) {
    window.location.reload(true)
  }

  const heading = [
    // {
    //   name: '#',
    //   selector: row => row.visitId,
    //   sortable: true,
    //   width: '0.6',

    // },
    {
      name: 'هوية الزائر',
      selector: row => row.vNationalId,
      sortable: true,

    },
    {
      name: 'اسم الزائر',
      selector: row => row.vName,
      sortable: true,
    },
    // {
    //   name: 'اسم الموظف',
    //   selector: row => row.eName,
    //   sortable: true,
    // },
    {
      name: 'رقم الطابق',
      selector: row => row.visitingFloor,
      sortable: true,

    },
    {
      name: 'تاريخ الزيارة',
      selector: row => row.date,
      sortable: true,
    },
    {
      name: 'وقت الزيارة',
      selector: row => row.time,
      sortable: true,
    },
    {
      name: 'وقت الدخول',
      selector: row => row.checkIn,
      cell: (row) => handleCheckInBtn(row),
      button: true,
      sortable: true,
    },
    {
      name: 'وقت الخروج',
      selector: row => row.checkOut,
      cell: (row) => handleCheckOutBtn(row),
      button: true,
      sortable: true,
    },
    // {
    //   name: 'Reject', 
    //   selector: row=> row.readOnly,
    //   cell: (row) => handleRejectBtn(row),
    //   button: true
    // }
  ]

  const conditionalRowStyles = [
    {
      when: row => row.checkIn != "00:00:00" && row.checkOut != "00:00:00",
      style: {
        backgroundColor: '#4B4F54',
        color: 'white',
        // highlightOnHover: 'false'
      },
    },
  ];

  const handleRejectBtn = (row) => {
    return <button onClick={() => { row.readOnly = true }}>Reject</button>
    // row.readOnly = true; 
  }

  const onCheck = (row, type) => {

    var visitId = row.visitId
    if (type === 'Checkin') {
      var req = axios.patch(
        apiAddress + 'Checkin/' + visitId,
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        }
      ).then(res => res.data).then(data => {
        setRefresh(true)
        return data
      }).catch(function (error) {
        return error
      })
    }
    else if (type === 'Checkout') {
      var req = axios.patch(
        apiAddress + 'Checkout/' + visitId,
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        }
      ).then(res => res.data).then(data => {
        setRefresh(true)
        return data
      }).catch(function (error) {
        return error
      })

    }
  }

  const handleCheckInBtn = (row) => {
    if (row.checkIn > "00:00:00")
      return row.checkIn
    else
      return <button
        // disabled={row.reject ? false : true}
        className='checkinBtn btn'
        onClick={() => {
          onCheck(row, "Checkin");
        }}>دخول</button>
  }

  const handleCheckOutBtn = (row) => {
    if (row.checkOut > "00:00:00")
      return row.checkOut
    else
      return <button
        className='checkoutBtn btn'
        disabled={row.checkIn > "00:00:00" || row.reject === true ? false : true}
        onClick={() => {
          onCheck(row, "Checkout")
        }}>خروج</button>
  }

  // const handleClearToggle = () => {
  //   setToggleClear(!toggleClear)
  // }

  const tabSelectHandler = (k) => {
    // setSelectedRows([])
    // setToggleClear(false)
    setTabKey(k)
    switch (k) {
      case 'todayVisits':
        setTableDataHolder(todayVisits)
        break;
      case 'futureVisits':
        setTableDataHolder(futureVisits)
        break;
      case 'pastVisits':
        setTableDataHolder(pastVisits)
        break;
      case 'allVisits':
        setTableDataHolder(data)
        break;
      default:
        break;
    }
  }

  return (
    <>
      <Tabs className="mb-3"
        id="controlled-tab-example"
        activeKey={tabKey}
        onSelect={(k) => tabSelectHandler(k)}
      >
        <Tab eventKey='todayVisits' title='زيارات اليوم'>
        </Tab>

        <Tab eventKey='futureVisits' title='الزيارات القادمة'>
        </Tab>

        <Tab eventKey='pastVisits' title='الزيارات الماضية' >
        </Tab>

        <Tab eventKey='allVisits' title='جميع الزيارات' >
        </Tab>
      </Tabs>
      <DataTable columns={heading} data={tableDataHolder} pagination
        selectableRows
        onSelectedRowsChange={(e) => handleRowSelected(e)}
        highlightOnHover={true}
        conditionalRowStyles={conditionalRowStyles}
        paginationPerPage={7}
      // clearSelectedRows={true}
      />
      {/* <Button text='Delete' color='red' onClick={deleteRows} /> */}
      <Buttons
        onAdd={onAdd}
        onDelete={() => { (selectedRows !== undefined && selectedRows !== null) ? onDelete() : alert('لم يتم اختيار زيارة للحذف') }}
        onEdit={() => { (selectedRows !== undefined && selectedRows !== null) ? onEdit() : alert('يجبب اختيار زيارة واحدة فقط للتعديل') }}
        onCheckin={() => { (selectedRows !== undefined && selectedRows !== null) ? onCheck('Checkin') : alert('يجبب اختيار زيارة واحدة فقط للتعديل') }}
        onCheckout={() => { (selectedRows !== undefined && selectedRows !== null) ? onCheck('Checkout') : alert('يجبب اختيار زيارة واحدة فقط للتعديل') }}
      />
    </>
  );
}


export default Datatable
