import React from 'react'
import VisitorForm from '../components/VisitorForm'
import CustomPopup from '../components/CustomPopup/CustomPopup'
import Popup from 'reactjs-popup';
import { useState, useEffect } from 'react'
import axios from 'axios';


const PopupForm = ({ token, setFormVisibility, formVisibility, updateData, updatePressed, setUpdatePressed }) => {
    const [visitorData, setVisitorData] = useState();
    const [visitData, setVisitData] = useState();
    const [validVisit, setValidVisit] = useState(false);
    const [validVisitor, setValidVisitor] = useState(false);
    const [refresh, setRefresh] = useState(false)
    const [clearData, setClearData] = useState(false)
    const [showSubmitButton, setShowSubmitButton] = useState(false)
    const apiAddress = global.config.i18n.visitApiAddress; 


    /**
     * Unsetting Data when form is closed
     */
    useEffect(() => {
        if (!formVisibility) {
            setVisitData(undefined);
            setVisitorData(undefined);
        }
        if (formVisibility) {
            setClearData(false)
        }
    }, [formVisibility])

    /**
     * Tracking the status of form data 
     */
    useEffect(() => {
        setClearData(false)
        if (!visitData && !visitorData) {
            setClearData(true)
        }

        if (visitData && visitorData) {
            setShowSubmitButton(true);
        }
    }, [visitData, visitorData])



    // const onClose = () => {
    //     setFormVisibility(false);
    //     setOpen(false);

    // }

    const handleSubmit = () => {
        if (!visitData && !visitorData) {
            alert('الرجاء تعبئة معلومات الزائر والزيارة')
        }
        else if (!visitData && visitorData) {
            alert('الرجاء تعبئة معلومات الزيارة')
        }
        else if (visitData && !visitorData) {
            alert('الرجاء تعبئة معلومات الزائر')
        }
        else {
            if (updatePressed)
                putSubmit()
            else
                postSubmit()
        }

    }

    const validateVisit = () => {
        // if(visitData)
        setValidVisit(false); 
        if(visitData.eId === "")
            alert('لم يتم ادخال رقم الموظف')
        if(visitData.visitingFloor === "") 
            alert('لم يتم ادخال رقم الطابق')
        if(visitData.date === "")
            alert('لم يتم ادخال تاريخ الزيارة')
        else if(visitData.date < new Date().toISOString().slice(0, 10))
            alert('التاريخ غير صحيح, يجب ادخال تاريخ في المستقبل')
        if(visitData.time === "")
            alert('لم يتم ادخال وقت الزيارة')
        else if(visitData.time < '07:00:00' || visitData.time > "16:00:00")
            alert('وقت الزيارة غير صحيح، يجب اختيار وقت ضمن اوقات الدوام الرسمي')
        else
            setValidVisit(true)
    }

    const validateVisitor = () => {
        // if(visitData)
        setValidVisitor(false); 
        if(visitorData.vNationalId === "") 
            alert('لم يتم ادخال هوية الزائر')
        else if(visitorData.vNationalId.length < 10)
            alert('الهوية يجب ان تتكون من 10 ارقام')
        if(visitorData.vName === "" || visitorData.vName === null)
            alert('لم يتم ادخال اسم الزائر')
        if(visitorData.vPhone === "" || visitorData.vPhone === null)
            alert('لم يتم ادخال جوال الزائر')
        if(visitorData.vEmail === "" || visitorData.vEmail === null)
            alert('لم يتم ادخال البريد الالكتروني للزائر')
        else
            setValidVisitor(true)
    }
    const postSubmit = async () => {
        // e.preventDefault();
        setRefresh(false)

        if (visitorData && visitData) {
            validateVisitor()
            validateVisit()
            if (validVisit && validVisitor) {
                var combinedData = { ...visitorData, ...visitData }
                await axios.post(
                    apiAddress,
                    combinedData,
                    {
                        headers: {
                            Authorization: `Bearer ${token}`
                        }
                    }
                ).then(function (response) {
                    if (response.status === 200) {
                        setRefresh(true)
                        return response
                    }
                    else {
                        alert('يوجد زائر في نفس اليوم')
                        return response
                    }
                }).catch(function (error) {
                    return error
                });
            }

        }
    }
    const putSubmit = async () => {
        setRefresh(false)
        var responseHolder
        if (visitorData) {
            await axios.put(
                apiAddress+'UpdateVisitor/' + updateData.vId,
                visitorData,
                {
                    headers: {
                        Authorization: `Bearer ${token}`
                    }
                }
            ).then(function (response) {
                return response;

            }).catch(function (error) {
                return error
            });
        }

        if (visitData) {
            await axios.put(
                apiAddress+ 'UpdateVisit/' + updateData.visitId,
                visitData,
                {
                    headers: {
                        Authorization: `Bearer ${token}`
                    }
                }
            ).then(function (response) {
                return response;

            }).catch(function (error) {
                return error
            });
        }
        setRefresh(true)
    }

    const popupCloseHandler = (e) => {
        setFormVisibility(e);
        setClearData(true);
        if (updatePressed)
            setUpdatePressed(false)
    };

    if (refresh) {
        window.location.reload(true);
    }
    return (
        <div>
            <CustomPopup
                onClose={popupCloseHandler}
                show={formVisibility}
                title="Hello Jeetendra"
            >
                <VisitorForm
                    token={token}
                    // onClose={(e) => setFormVisibility(false)}
                    method='post'
                    updateData={updateData}
                    updatePressed={updatePressed}
                    setVisitData={setVisitData}
                    setVisitorData={setVisitorData}
                    clearData={clearData}
                    showSubmitButton={showSubmitButton}
                    onSubmit={handleSubmit}
                    visitData={visitData}
                    visitorData={visitorData}
                />
            </CustomPopup>
        </div>
    )
}

export default PopupForm