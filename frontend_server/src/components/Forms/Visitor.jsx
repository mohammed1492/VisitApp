import React from 'react'
import { useState, useEffect } from 'react'
import axios from 'axios'
import Button from '../Button'

const Visitor = ({ onDisable, token, setVisitorData, clearData, visitorData }) => {

    const [visitorExist, setVisitorExist] = useState(false)
    const [vName, setVName] = useState()
    const [vNationalId, setNationalId] = useState()
    const [vPhone, setPhone] = useState('')
    const [vEmail, setEmail] = useState('')
    const [allFieldsFilled, setAllFieldsFilled] = useState(false)

    useEffect(() => {
        if (!visitorExist) {
            setVName('')
            setPhone('')
            setEmail('')
        }
    }, [visitorExist]);

    useEffect(() => {
        if (clearData) {
            setVName('')
            setEmail('')
            setPhone('')
            setNationalId('')
            setVisitorExist(false)
        }
    }, [clearData])

    useEffect(()=>{
        if(visitorData)
            fillVisitorStates()
    }, [visitorData])

    useEffect(() =>{
        // set The data state only when data is enterd
        if(vNationalId || vName || vEmail || vPhone){
            setVisitorData({
                vName, vNationalId, vPhone, vEmail
            })
        }
        else
            setVisitorData(undefined)

    }, [vNationalId, vName, vEmail, vPhone])

    const fetchVisitorInfo = async () => {
        setVisitorExist(false)
        await axios.get(
            'http://localhost:5000/api/Visit/GetVisitor/' + vNationalId,
            {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            }
        ).then(res => res.data).then(data => {
            setVisitorExist(true)
            setVName(data[0].name)
            // console.log(data[0].name)
            setPhone(data[0].phone)
            setEmail(data[0].email)
            return data
        }).catch(function (error) {
            return error
        });
    }

    const fillVisitorStates = () => {
        setNationalId(visitorData.vNationalId)
        setVName(visitorData.vName)
        // console.log(data[0].name)
        setPhone(visitorData.vPhone)
        setEmail(visitorData.vEmail)
    }



    const handleKeyDown = (event) => {
        if (event.key === 'Enter' || event.key === 'Tab') {
            var visitorData = fetchVisitorInfo();

        }
    }

    // const saveVisitor = () => {
    //     if (!vName || !vNationalId || !vPhone || !vEmail) {
    //         alert('الرجاء تعبئة جميع الحقول المطلوبة')
    //     }
    //     else {
    //         setVisitorData({
    //             vName, vNationalId, vPhone, vEmail
    //         })
    //     }

    // }
    return (
        <>
            <form className='login-form' >
                <label htmlFor="nationalId"><span className='astrisk'>*</span>رقم الهوية </label>
                <input type="nationalId" id="nationalId" value={vNationalId} onChange={(e) => setNationalId(e.target.value)}
                    onKeyDown={handleKeyDown} required />
                <label htmlFor="vName"><span className='astrisk'>*</span>الاسم </label>
                <input type="text" id="vName" value={vName} onChange={(e) => { !visitorExist && setVName(e.target.value) }} disabled={visitorExist} required />
                <label htmlFor="phone"><span className='astrisk'>*</span>رقم جوال </label>
                <input type="text" id="phone" value={vPhone} onChange={(e) => setPhone(e.target.value)} disabled={visitorExist} required />
                <label htmlFor="email"><span className='astrisk'>*</span>البريد الالكتروني </label>
                <input type="email" id="email" value={vEmail} onChange={(e) => setEmail(e.target.value)} disabled={visitorExist} required />
            </form>
            {/* <Button text='Save' color='Blue' onClick={() => saveVisitor()} /> */}
        </>
    )
}

export default Visitor