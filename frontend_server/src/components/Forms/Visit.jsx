import React from 'react'
import { useState, useEffect } from 'react'
import Button from '../Button'
import TimePicker from 'rc-time-picker';
import 'rc-time-picker/assets/index.css';

const Visit = ({ token, setVisitData, clearData, visitData }) => {
    const [companions, setCompanions] = useState()
    const [visitingFloor, setVisitingFloor] = useState()
    const [date, setDate] = useState('')
    const [time, setTime] = useState('')
    const [eId, setEId] = useState()

    useEffect(() => {
        if (clearData) {
            setVisitingFloor('')
            setDate('')
            setTime('')
            setEId('')
            setCompanions('')
        }
    }, [clearData])

    useEffect(() => {
        if (visitData)
            fillVisitorStates()
    }, [visitData])

    useEffect(() => {

        if (eId || date || time || eId)
            setVisitData({
                visitingFloor, date, time, eId
            })
        else
            setVisitData(undefined)

    }, [visitingFloor, date, time, eId])

    const fillVisitorStates = () => {
        setEId(visitData.eId)
        setDate(visitData.date)
        // console.log(data[0].name)
        setTime(visitData.time)
        setVisitingFloor(visitData.visitingFloor)
    }

    var options = [];
    for (var i = 1; i < 16; i++) {
        options.push({ label: i, value: i })
    }
    return (
        <>
            <label htmlFor="eId"><span className='astrisk'>*</span>الرقم الوظيفي للموظف</label>
            <input type="number" id="eId" value={eId} onChange={(e) => setEId(e.target.value)} required />
            {/* <label htmlFor="companions">عدد المرافقين</label>
            <input type="number" id="companions" value={companions} onChange={(e) => setCompanions(e.target.value)} /> */}
            <label htmlFor="floor"><span className='astrisk'>*</span>رقم الطابق</label>
            <select id="floor" value={visitingFloor} onChange={(e) => setVisitingFloor(e.target.value)}>
                <option value="" selected>اختر رقم الطابق</option>
                {options.map(
                    (option) => (
                        <option value={option.value}>{option.label}</option>
                    )
                )}
            </select>
            <label htmlFor="date"><span className='astrisk'>*</span>التاريخ</label>
            <input type="date" id="date" value={date} onChange={(e) => setDate(e.target.value)} />
            <label htmlFor="time"><span className='astrisk'>*</span>الوقت</label>
            <TimePicker
                showSecond={false}
                defaultValue={time}
                className=""
                onChange={(value) => setTime(value && value.format('HH:mm:00'))}
                use12Hours
                inputReadOnly
            />
            {/* <input type="time" step='1' id="time" value={time} onChange={(e) => setTime(e.target.value)} /> */}
            {/* <Button text='Save' color='Blue' onClick={(e) => setVisitData({
                visitingFloor, date, time, eId
            })}/> */}
        </>

    )
}

export default Visit