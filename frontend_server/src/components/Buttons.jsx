import React from 'react'
import Button from './Button'

const Buttons = ({onAdd, onDelete, onEdit, onCheckin, onCheckout}) => {
  return (
    <div className='buttons'>
        <Button text='زيارة جديدة' color='#005EB8' onClick={onAdd}/> 
        <Button text='تعديل زيارة' color='#4B4F54' onClick={onEdit}/>
        <Button text='حذف زيارة' color='#E5554F' onClick={onDelete}/> 
        {/* <Button text='دخول' color='blue' onClick={onCheckin}/>
        <Button text='خروج' color='black' onClick={onCheckout}/> */}
    </div>
  )
}



export default Buttons