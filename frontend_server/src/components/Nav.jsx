import React from 'react'
import Button from './Button'
import Logo from "../logo-en.png"



const Nav = ({search}) => {
    const [searchText, setSearchText] = React.useState()
    return (
        <nav class="navbar navbar-expand-lg bg-light">
            <div class="container-fluid">
                {/* <a class="navbar-brand" >زيارة</a> */}
                <img src={Logo} className='' width="10%" />
                {/* <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon">Hi</span>
                </button> */}
                <div class="d-flex">

                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link" href="/login">تسجيل دخول</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">تسجيل </a>
                        </li>
                    </ul>

                </div>
            </div>
        </nav>
    )
}

export default Nav