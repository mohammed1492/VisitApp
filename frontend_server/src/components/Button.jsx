import React from 'react'

const Button = ({ id, color, text, onClick}) => {
  return (
    <div>
        <button id={id} className='btn' style={{backgroundColor: color, color: 'white' }}
        onClick={onClick}>{text}</button>
    </div>
  )
}

export default Button