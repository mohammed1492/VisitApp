import axios from 'axios'
import React, { useDebugValue, useEffect, useState } from 'react'
import { Navigate } from 'react-router-dom'
import { Tab, Tabs } from 'react-bootstrap'
import Visitor from './Forms/Visitor'
import Visit from './Forms/Visit'
import Button from './Button'

const VisitorForm = ({ token, updateData, updatePressed, onSubmit, setVisitData, setVisitorData, showSubmitButton, clearData, visitorData, visitData }) => {
    // const [activeTab, setActiveTab] = useState('visitor');

    // const sendHttpPost = async (data) => {
    //     console.log(data)
    // }


    useEffect(() => {
        if (updateData) {
            setVisitData(
                (
                    ({ eId, visitingFloor, date, time }) => ({ eId, visitingFloor, date, time })
                )(updateData)
            )
            setVisitorData(
                (
                    ({ vName, vNationalId, vPhone, vEmail }) => ({ vName, vNationalId, vPhone, vEmail })
                )(updateData)
            )
        }

    }, [updateData, updatePressed])

    return (
        <div className='container login-container'>
            <div className='form-control'>
                <Tabs className="mb-3" >
                    <Tab eventKey='visitor' title="معلومات الزائر">
                        <Visitor token={token} setVisitorData={setVisitorData} clearData={clearData} visitorData={visitorData} />
                        {/* <Button text='next' color='blue' onSubmit={e => toNextTab(e)}/> */}
                    </Tab>
                    <Tab eventKey='visit' title='معلومات الزيارة'>
                        <Visit token={token} setVisitData={setVisitData} clearData={clearData} visitData={visitData} />
                    </Tab>
                </Tabs>
            </div>
            <Button id='submitForm' text='إرسال' color='#005EB8' onClick={() => onSubmit()} />
        </div>
    )

}


export default VisitorForm