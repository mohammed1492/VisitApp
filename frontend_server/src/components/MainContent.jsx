import React from 'react'
import { useState } from 'react'
import Table from './Datatable'
import PopupForm from './PopupForm'

const MainContent = ({ data, heading, tokenString, fetchTable }) => {
    const [formVisibility, setFormVisibility] = useState(false)
    const [formUpdateData, setFormUpdateData] = useState()
    const [updatePressed, setUpdatePressed] = useState(false)
    return (
        <div>
            <Table
                token={tokenString}
                data={data}
                heading={heading}
                refreshData={fetchTable}
                setFormVisibility={setFormVisibility}
                setFormData={setFormUpdateData}
                setUpdatePressed={setUpdatePressed}
            />
            <PopupForm
                token={tokenString}
                setFormVisibility={setFormVisibility}
                formVisibility={formVisibility}
                updateData={formUpdateData}
                updatePressed={updatePressed}
                setUpdatePressed={setUpdatePressed}
            />
        </div>
    )
}

export default MainContent